import urllib.request
import re
import os.path

path_of_script = os.path.realpath(__file__)
path_of_folder = os.path.dirname(path_of_script) + '/imgs/'


def main():
	right_name = False
	while not right_name:
		try:
			instagram_name = input("Enter desired instagram account name:")

			headers = {'User-Agent': 'Mozilla/5.0 (WINDOWS NT 6.1; Win64; x64'}
			req = urllib.request.Request('https://www.instagram.com/{0}/'.format(instagram_name), headers=headers)

			with urllib.request.urlopen(req) as response:
				the_page = response.read()

		except:
			print("Wrong account name, try again")
			continue

		right_name = True
		print("Images will be saved...")

	decoded_page = the_page.decode('utf-8')

	start_string = 'https://scontent'

	end_string = '=scontent-frt3-2.cdninstagram.com'

	paragraphs = re.findall(r'{0}(.*?){1}'.format(start_string, end_string), str(decoded_page))
	#paragraphs = re.findall(r'{0}((?!s150x150).)(.*?){1}'.format(start_string, end_string), str(decoded_page))
	#paragraphs = re.findall(r'{0}(.*?){1}'.format(start_string, end_string), str(decoded_page))

	full_urls = []
	if not os.path.isdir(path_of_folder):
		os.mkdir(path_of_folder)

	for paragraph in paragraphs:

		if not "s150x150" in paragraph:

			full_url = start_string + paragraph + end_string
			full_urls.append(full_url)

	num = 0
	for url in full_urls:
		name = path_of_folder + str(num) + '.jpg'
		urllib.request.urlretrieve(url, name)
		num += 1

	print("Images saved.")

if __name__ == '__main__':
	main()
	print(path_of_folder)
